<%-- 
    Document   : createNewAccount
    Created on : Jun 12, 2018, 8:53:56 AM
    Author     : PhuNDSE63159
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<html>
    <head>
        <title>Register</title>
        <s:head/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Create new Account</h1>
        <s:form action="create" method="POST">
            <s:textfield name="username"  label="Username"/>
            <s:textfield name="password" label="Password" />
            <s:textfield name="confirm" label="Confirm" />
            <s:textfield name="fullname"  label="Fullname" />
            <s:submit value="Register"/>
            <s:reset/>
        </s:form>
        <s:if test="%{exception.message.contains('duplicate')}">
            <font color="red">
            <s:property value="username"/> is Exsisted !!!
            </font>
        </s:if>
    </body>
</html>
