/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.struts.search;

import java.util.List;
import phund.tblusers.tblUsersDAO;
import phund.tblusers.tblUsersDTO;

/**
 *
 * @author PhuNDSE63159
 */
public class SearchAction {

    private String searchValue;
    private List<tblUsersDTO> listAccount;
    private final String SUCCESS = "success";

    public SearchAction() {
    }

    public String execute() throws Exception {
        tblUsersDAO dao = new tblUsersDAO();
        System.out.println("Search value " + searchValue);
        dao.searchLastName(searchValue);
        this.listAccount = dao.getList();
        System.out.println(listAccount.size());
        return SUCCESS;
    }

    /**
     * @return the searchValue
     */
    public String getSearchValue() {
        return searchValue;
    }

    /**
     * @param searchValue the searchValue to set
     */
    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    /**
     * @return the listAccount
     */
    public List<tblUsersDTO> getListAccount() {
        return listAccount;
    }

    /**
     * @param listAccount the listAccount to set
     */
    public void setListAccount(List<tblUsersDTO> listAccount) {
        this.listAccount = listAccount;
    }

}
