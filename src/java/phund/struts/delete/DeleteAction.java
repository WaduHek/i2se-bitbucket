/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.struts.delete;

import phund.tblusers.tblUsersDAO;

/**
 *
 * @author PhuNDSE63159
 */
public class DeleteAction {

    private String username;
    private String searchValue;
    private final String SUCCESS = "success";
    private final String FAIL = "fail"; // trường hợp ERROR

    public DeleteAction() {
    }

    public String execute() throws Exception {
        String url = FAIL;
        tblUsersDAO dao = new tblUsersDAO();
        System.out.println("username " + username);
        System.out.println("searchValue " + searchValue);
        boolean res = dao.deleteAccount(username);
        if (res) {
            url = SUCCESS;
        }
        return url;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the searchValue
     */
    public String getSearchValue() {
        return searchValue;
    }

    /**
     * @param searchValue the searchValue to set
     */
    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

}
