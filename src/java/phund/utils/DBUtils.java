/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.utils;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author PhuNDSE63159
 */
public class DBUtils implements Serializable {

    /**
     * Code tĩnh Ngoài ra còn code động, có thể sửa linh hoạt hơn
     *
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Connection makeConnection() throws /*ClassNotFoundException, SQLException,*/ NamingException, SQLException {
//        //load driver
//        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//        //tao string url
//        String url = "jdbc:sqlserver://localhost:1433;databaseName=LOGINDEMO";
//        //Open connection
//        Connection c = DriverManager.getConnection(url, "sa", "12345678");
//        return c;

//B1: Lấy context hiện hành
        Context context = new InitialContext();
        //B2: Lấy context server.
        Context tomcatCtx = (Context) context.lookup("java:comp/env");
        DataSource ds = (DataSource) tomcatCtx.lookup("Struts2");
        Connection c = ds.getConnection();
        return c;
    }

}
